"use strict";


var songs = [
    {
        'id': 1,
        'author': 'Timbaland',
        'name': 'Apologize',
        'ganre': 'pop',
        'year': 2007
    },
    {'id': 2, 'author': 'Timbaland', 'name': 'Timbaland - The Way I Are ft. Keri Hilson', 'ganre': 'pop', 'year': 2007},
    {'id': 3, 'author': 'Timbaland', 'name': 'Morning After Dark', 'ganre': 'pop', 'year': 2008},
    {'id': 4, 'author': 'Timbaland', 'name': 'Carry Out', 'ganre': 'pop', 'year': 2008},
    {'id': 5, 'author': 'Timbaland', 'name': 'Say Something {Clean}', 'ganre': 'pop', 'year': 2008},
    {'id': 6, 'author': 'Timbaland', 'name': 'Scream - Radio Edit', 'ganre': 'pop', 'year': 2007},
    {'id': 7, 'author': 'Timbaland', 'name': 'If We Ever Meet Again', 'ganre': 'pop', 'year': 2009},
    {'id': 8, 'author': 'Justin Timberlake', 'name': 'What Goes Around...Comes Around', 'ganre': 'pop', 'year': 2009},
    {
        'id': 9,
        'author': 'Justin Timberlake',
        'name': 'CAN\'T STOP THE FEELING! (Film Version)',
        'ganre': 'pop',
        'year': 2016
    },
    {'id': 10, 'author': 'Justin Timberlake', 'name': 'Mirrors', 'ganre': 'pop', 'year': 2013},
    {
        'id': 11,
        'author': 'Justin Timberlake',
        'name': 'Say Something (feat. Chris Stapleton)',
        'ganre': 'pop',
        'year': 2018
    },
    {'id': 12, 'author': 'Justin Timberlake', 'name': 'Love Never Felt So Good', 'ganre': 'pop', 'year': 2014},
    {'id': 13, 'author': 'Justin Timberlake', 'name': 'SexyBack { Clean}', 'ganre': 'pop', 'year': 2010},
    {'id': 14, 'author': 'Nickelback', 'name': 'How You Remind Me', 'ganre': 'rock', 'year': 2010},
    {'id': 15, 'author': 'Nickelback', 'name': 'Far Away', 'ganre': 'rock', 'year': 2010},
    {'id': 16, 'author': 'Nickelback', 'name': 'Rockstar', 'ganre': 'rock', 'year': 2007},
    {'id': 17, 'author': 'Nickelback', 'name': 'Savin\' Me', 'ganre': 'rock', 'year': 2010},
    {'id': 18, 'author': 'Nickelback', 'name': 'If Today Was Your Last Day', 'ganre': 'rock', 'year': 2010},
    {'id': 19, 'author': 'Nickelback', 'name': 'Never Gonna Be Alone', 'ganre': 'rock', 'year': 2010},
    {'id': 20, 'author': 'Beyoncé', 'name': 'Halo', 'ganre': 'hip-hop', 'year': 2010},
    {'id': 21, 'author': 'Beyoncé', 'name': 'APESHIT', 'ganre': 'hip-hop', 'year': 2018},
    {'id': 22, 'author': 'Beyoncé', 'name': 'Crazy in Love', 'ganre': 'hip-hop', 'year': 2010},
    {'id': 23, 'author': 'Beyoncé', 'name': 'If I Were a Boy', 'ganre': 'hip-hop', 'year': 2010},
    {'id': 24, 'author': 'Beyoncé', 'name': 'Drunk in Love {Explicit}', 'ganre': 'hip-hop', 'year': 2014},
    {'id': 25, 'author': 'Beyoncé', 'name': 'Love on Top', 'ganre': 'hip-hop', 'year': 2012},
    {'id': 26, 'author': 'Beyoncé', 'name': 'Single Ladies (Put a Ring on It)', 'ganre': 'hip-hop', 'year': 2010},
    {'id': 25, 'author': 'Beyoncé', 'name': 'Run the World (Girls)', 'ganre': 'hip-hop', 'year': 2011}
];


class Row extends React.Component {
    constructor() {
        super();
    }

    render() {
        var author = this.props.data.author,
            name = this.props.data.name,
            ganre = this.props.data.ganre,
            year = this.props.data.year

        return (
            <div className='row'>
                <div className='row__author'>{author}</div>
                <div className='row__name'>{name}</div>
                <div className='row__ganre '>{ganre}</div>
                <div className='row__year '>{year}</div>
            </div>
        )
    }
}


class Pagination extends React.Component {
    render() {
        var page = this.props.page;
        var counts = [];
        var len = this.props.data;
        var relate = Math.ceil(len / this.props.soundsCount);
        for (var i = 1; i <= relate; i++) {
            counts.push(i);
        }
        var pages = counts.map(function (res, key) {
            return (
                <li key={key} className={(page == res ? 'active' : '')} data-page={res}>{res}</li>
            )
        });

        return (
            <ul className="pagination">
                <li onClick={this.onList} className={"prev" + (page == 1 ? ' hidden' : '')}>
                    <i className="prev fa fa-angle-double-left"
                       aria-hidden="true"></i>
                </li>
                {pages}
                <li onClick={this.onList} className={"next" + (page == relate ? ' hidden' : '')}>
                    <i className="fa fa-angle-double-right next"
                       aria-hidden="true"></i>
                </li>
            </ul>
        )
    }
}

class Counts extends React.Component {
    render() {
        return (
            <ul>
                <li className="active" onClick={() => {
                    this.props.rechange(10)
                }}>10
                </li>
                <li onClick={() => {
                    this.props.rechange(25)
                }}>25
                </li>
                <li onClick={() => {
                    this.props.rechange(50)
                }}>50
                </li>
                <li onClick={() => {
                    this.props.rechange(100)
                }}>100
                </li>
            </ul>
        )
    }
}

class Filter extends React.Component {
    constructor(data) {
        super();
        this.state = {
            data: data.data
        }
    }

    uniqueAuth() {
        var result = this.state.data.map(function (obj) {
            return (obj.author)
        });
        let unique = [...new Set(result)];
        return unique;
    }

    uniqueGanre() {
        var result = this.state.data.map(function (obj) {
            return (obj.ganre)
        });
        let unique = [...new Set(result)];
        return unique;
    }

    uniqueYear() {
        var result = this.state.data.map(function (obj) {
            return (obj.year)
        });
        let unique = [...new Set(result)];
        return unique;
    }

    render() {
        var tmp = this.uniqueAuth();
        let fields = ['author', 'name', 'ganre', 'year'];
        let authors = tmp.map(function (author, index) {
            return (<option key={index}>{author}</option>)
        });
        tmp = this.uniqueGanre();
        let ganres = tmp.map(function (ganre, index) {
            return (<option key={index}>{ganre}</option>)
        });
        tmp = this.uniqueYear();
        let years = tmp.map(function (year, index) {
            return (<option key={index}>{year}</option>)
        });
        return (
            <form id="filter">
                <select name="author">
                    <option value="">Все</option>
                    {authors}</select>
                <select name="ganre">
                    <option value="">Все</option>
                    {ganres}</select>
                <select name="year">
                    <option value="">Все</option>
                    {years}
                </select>
                <input type="button" value="Подобрать" onClick={this.props.filter}/>
            </form>
        );
    }
}

class Table extends React.Component {
    constructor(data) {
        super();
        this.state = {
            songs: data.data,
            page: 1,
            soundsCount: 10,
            defaultSounds: data.data
        };
        this.compareYear = this.compareYear.bind(this);
        this.compareName = this.compareName.bind(this);
        this.compareAuthor = this.compareAuthor.bind(this);
        this.compareGanre = this.compareGanre.bind(this);
        this.sortUp = this.sortUp.bind(this);
        this.sortDown = this.sortDown.bind(this);
        this.changeSongs = this.changeSongs.bind(this);
        this.filter = this.filter.bind(this);
        this.onList = this.onList.bind(this);
        this.addSort = this.addSort.bind(this);
        this.updateCount = this.updateCount.bind(this);
    }

    compareYear(song1, song2) {
        return song1.year - song2.year;
    }

    compareName(song1, song2) {
        var songF = song1.name.toLowerCase();
        var songS = song2.name.toLowerCase();
        var res;
        for (var i = 0; i < Math.max(songF.length, songS.length); i++) {
            for (var j = 0; j < Math.max(songF.length, songS.length); j++) {
                if (songF[i] > songS[i])
                    return 1;
                else if (songF[i] < songS[i])
                    return -1;
                else if (songF[i] == songS[i])
                    res = 0;
            }
        }
        return res;
    }

    compareAuthor(song1, song2) {
        var songF = song1.author.toLowerCase();
        var songS = song2.author.toLowerCase();
        var res;
        for (var i = 0; i < Math.max(songF.length, songS.length); i++) {
            for (var j = 0; j < Math.max(songF.length, songS.length); j++) {
                if (songF[i] > songS[i])
                    return 1;
                else if (songF[i] < songS[i])
                    return -1;
                else if (songF[i] == songS[i])
                    res = 0;
            }
        }
        return res;
    }

    compareGanre(song1, song2) {
        var songF = song1.ganre.toLowerCase();
        var songS = song2.ganre.toLowerCase();
        var res;
        for (var i = 0; i < Math.max(songF.length, songS.length); i++) {
            for (var j = 0; j < Math.max(songF.length, songS.length); j++) {
                if (songF[i] > songS[i])
                    return 1;
                else if (songF[i] < songS[i])
                    return -1;
                else if (songF[i] == songS[i])
                    res = 0;
            }
        }
        return res;
    }

    addSort(field){
        if (field == "name"){
            let data = this.state.songs;
            var fun = this.compareAuthor;
            var res = this.state.songs.reduce(function(prev, curr, i, arr ){
                if (prev.name == curr.name && !start)
                    var start = i;
                if (prev.name != curr.name) {
                    if (!start)
                        start = i - 1;
                    let end = i;
                    let tmp = data.slice(start, end);
                    console.log(fun);
                    tmp.sort(fun);
                    return tmp;
                    start = null;
                    end = null;

                }
            });
        }
        console.log(res);

    }

    sortUp(e) {
        var by = e.target.getAttribute('data-by');
        switch (by) {
            case 'name':
                this.state.songs.sort(this.compareName);
                //this.addSort('name');
                break;
            case 'author':
                this.state.songs.sort(this.compareAuthor);
                //this.state.songs.sort(this.compareName);
                break;
            case 'ganre':
                this.state.songs.sort(this.compareGanre);
                //this.state.songs.sort(this.compareName);
                break;
            case 'year':
                this.state.songs.sort(this.compareYear);
                //this.state.songs.sort(this.compareName);
                break;
        }
        this.setState({
            songs: this.state.songs,
            page: this.state.page,
            soundsCount: this.state.soundsCount
        });
    }

    sortDown(e) {
        var by = e.target.getAttribute('data-by');
        switch (by) {
            case 'name':
                this.state.songs.sort(this.compareName);
                break;
            case 'author':
                this.state.songs.sort(this.compareAuthor);
                break;
            case 'ganre':
                this.state.songs.sort(this.compareGanre);
                break;
            case 'year':
                this.state.songs.sort(this.compareYear);
                break;
        }
        this.state.songs.reverse();
        this.setState({
            songs: this.state.songs,
            page: this.state.page,
            soundsCount: this.state.soundsCount
        });
    }

    changeSongs(value) {
        this.setState({
            songs: value
        })
    }

    filter(e) {
        let cancel = this.state.defaultSounds.filter(function (song) {
            if (document.getElementById('filter')[0].value == "" || song.author == document.getElementById('filter')[0].value) {
                if (document.getElementById('filter')[1].value == "" || song.ganre == document.getElementById('filter')[1].value) {
                    if (document.getElementById('filter')[2].value == "" || song.year == document.getElementById('filter')[2].value) {
                        return true;
                    }
                }
            }
        });
        //this.state.songs = cancel;
        this.setState({
            songs: cancel,
            soundsCount: this.state.soundsCount,
        })
    }

    onList(e) {
        if (e.target.tagName == "I" || e.target.tagName == "LI") {

            if (e.target.getAttribute('data-page')) {

                var nPage = e.target.getAttribute('data-page');
                this.setState({
                    page: nPage,
                    songs: this.state.songs,
                    soundsCount: this.state.soundsCount
                });
            } else if (e.target.classList.contains('next')) {
                this.setState({page: this.state.page + 1});
            } else if (e.target.classList.contains('prev')) {
                this.state.page == 1 ? "" : this.setState(
                    {
                        page: (this.state.page - 1)
                    });
            }
        }
    }

    updateCount(value) {
        var el = document.getElementById('counts').children[0];
        var par = el.children;
        for (var i = 0; i < par.length; i++) {
            par[i].classList.remove('active');
        }
        for (var i = 0; i < par.length; i++) {
            if (par[i].innerText == value)
                par[i].classList.add('active');
        }

        this.setState({
            page: 1,
            songs: this.state.songs,
            soundsCount: value
        })
    }

    render() {
        if (!this.state.page)
            var pageData = this.state.songs.slice((this.state.page * this.state.soundsCount, (this.state.page + 1) * this.state.soundsCount));
        else
            var pageData = this.state.songs.slice((this.state.page - 1) * this.state.soundsCount, this.state.page * this.state.soundsCount);
        var tableTemplate;
        if (pageData.length) {
            tableTemplate = pageData.map(function (key, index) {
                return (

                    <div key={index}>
                        <Row data={key}/>
                    </div>
                )
            });
        }
        else {
            tableTemplate = <p>К сожалению песен нет</p>;
        }
        return (
            <div className="app">
                <div className="col-md-8 col-sm-12">
                    <h2>Плейлист</h2>
                    <div className="table">
                        <div className='row'>
                            <div className='row__author'>АВТОР
                                <span className="icons">
                                <i data-by="author" onClick={this.sortUp} className="fa fa-caret-up "
                                   aria-hidden="true"></i>
                                <i data-by="author" onClick={this.sortDown} className="fa fa-caret-down"
                                   aria-hidden="true"></i>
                        </span>
                            </div>
                            <div className='row_name'>НАЗВАНИЕ
                                <span className="icons">
                                <i data-by="name" onClick={this.sortUp} className="fa fa-caret-up "
                                   aria-hidden="true"></i>
                                <i data-by="name" onClick={this.sortDown} className="fa fa-caret-down"
                                   aria-hidden="true"></i>
                        </span>
                            </div>
                            <div className='row__ganre '>ЖАНР
                                <span className="icons">
                                <i data-by="ganre" onClick={this.sortUp} className="fa fa-caret-up "
                                   aria-hidden="true"></i>
                                <i data-by="ganre" onClick={this.sortDown} className="fa fa-caret-down"
                                   aria-hidden="true"></i>
                        </span>
                            </div>
                            <div className='row__year '>ГОД
                                <span className="icons">
                                <i data-by="year" onClick={this.sortUp} className="fa fa-caret-up "
                                   aria-hidden="true"></i>
                                <i data-by="year" onClick={this.sortDown} className="fa fa-caret-down"
                                   aria-hidden="true"></i>
                        </span>
                            </div>
                        </div>
                        {tableTemplate}

                        <strong className="result {data.length > 0 ? '':'none'}">Всего песен: {songs.length} </strong>
                        <div className="last-block">
                            <div onClick={this.onList} className="paginate">
                                <Pagination soundsCount={this.state.soundsCount} page={this.state.page}
                                            data={this.state.songs.length}/>
                            </div>
                            <div className="counts" id="counts">
                                <Counts soundsCount={this.state.soundsCount} rechange={this.updateCount}/>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="col-md-4 col-sm-12">
                    <h2>Фильтр</h2>
                    <Filter data={songs} filter={this.filter}/>
                </div>
            </div>
        )
            ;
    }
}


class App extends React.Component {
    render() {
        return (
            <div className="container app">
                <Table data={songs}/>
            </div>
        );
    }
}


ReactDOM.render(
    <App/>,
    document.getElementById('root')
);